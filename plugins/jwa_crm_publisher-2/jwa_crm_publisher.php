<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              htts://justwebagency.com
 * @since             1.0.0
 * @package           Jwa_crm_publisher
 *
 * @wordpress-plugin
 * Plugin Name:       JWA CRM Publisher
 * Plugin URI:        htts://justwebagency.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Ivan Karpushchenko
 * Author URI:        htts://justwebagency.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       jwa_crm_publisher
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'JWA_CRM_PUBLISHER_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-jwa_crm_publisher-activator.php
 */
function activate_jwa_crm_publisher() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-jwa_crm_publisher-activator.php';
	Jwa_crm_publisher_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-jwa_crm_publisher-deactivator.php
 */
function deactivate_jwa_crm_publisher() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-jwa_crm_publisher-deactivator.php';
	Jwa_crm_publisher_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_jwa_crm_publisher' );
register_deactivation_hook( __FILE__, 'deactivate_jwa_crm_publisher' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-jwa_crm_publisher.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_jwa_crm_publisher() {

	$plugin = new Jwa_crm_publisher();
	$plugin->run();

}
run_jwa_crm_publisher();
