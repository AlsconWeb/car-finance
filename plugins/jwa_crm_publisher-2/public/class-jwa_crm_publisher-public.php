<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       htts://justwebagency.com
 * @since      1.0.0
 *
 * @package    Jwa_crm_publisher
 * @subpackage Jwa_crm_publisher/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Jwa_crm_publisher
 * @subpackage Jwa_crm_publisher/public
 * @author     Ivan Karpushchenko <theblackkarps@gmail.com>
 */
class Jwa_crm_publisher_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->crmFields = array(
            // Lead/Deal fields
            'firstName',
            'lastName',
            'phone',
            'email',
            'bday',
            'address',
            'monthlyIncome',
            'sin',
            'creditScore',
            'vehicleType',
            // Vehicle fields
            'dealerName',
            'vin',
            'year',
            'make',
            'model',
            'trim',
            'price',
            'link',
        );
        $this->post_url = 'https://carsoft.ca/webRequest/process';
        $this->token = 'E7BD936FFC5CACC82EDC0B10BF3356CB';
        $this->key = 'G1865FB0E1EFD7NK886D5F2WR7E48334';
        $this->reqType = 'INSERT';
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Jwa_crm_publisher_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Jwa_crm_publisher_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/jwa_crm_publisher-public.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Jwa_crm_publisher_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Jwa_crm_publisher_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/jwa_crm_publisher-public.js', array('jquery'), $this->version, false);
    }

    public function cf7_crm_submit($contact_form)
    {
        $submission = WPCF7_Submission::get_instance();
        // Get the post data and other post meta values.
        if ($submission) {
            $crmFields = $this->crmFields;
            $token = $this->token;
            $key = $this->key;
            $reqType = $this->reqType;
            if ($_POST['_wpcf7'] == '5035') {
                $trxType = 'DEAL';
            } else {
                $trxType = 'LEAD';
            }
            $trxType = 'LEAD';

            $body = array(
                'key' => $key,
                'token' => $token,
                'reqType' => $reqType,
                'trxType' => $trxType,
            );

            $body["notes"] = '';

            $posted_data = $submission->get_posted_data();
            $body['firstName'] = $posted_data["your-name"];
            $body['lastName'] = 'Doe';
            $body['phone'] = $posted_data["your-phone"];
            $body['email'] = "email" . rand() . "@email.ca";
            $body['notes'] .= "Date: " . $posted_data['date_select'] . '/n' . 'Time select: ' . $posted_data['time_select'] . 'Message: ' . $posted_data['your-message'];

            foreach ($posted_data as $key => $field) {
                if (in_array($key, $crmFields)) {
                    $body[$key] = $field;
                }
            }

            // var_dump($body);
            $this->push_to_crm($body);
            // var_dump($posted_data);
        }
    }

    public function gf_crm_submit($entry, $form)
    {
        // print_r($entry);
        $crmFields = $this->crmFields;
        $token = $this->token;
        $key = $this->key;
        $reqType = $this->reqType;
        $trxType = 'LEAD';

        $body = array(
            'key' => $key,
            'token' => $token,
            'reqType' => $reqType,
            'trxType' => $trxType,
        );
        $body["notes"] = '';

        $label = [
            'What’s your price range:',
            'What Is Your Estimated Credit Rating?',
            'What’s your price range:',
            'What Is Your Monthly Budget?',
            'Do you intend to put down payment?',
            'Down payment',
            'Are You Currently Driving a Vehicle?',
            'Approximate value of your trade-in vehicle',
            'Have you found a vehicle already?',
            'Vehicle Year',
            'Price',
            'Mileage',
            'City',
            'Address',
            'City',
            'Postal Code',
            'Province',
            'Years',
            'Months',
            'Do You Rent or Own?',
            'Monthly Payment',
            'What Is Your Employment Status?',
            'What Is Your Income Source?',
            'Monthly Income Before Taxes or Deductions',
            'Years',
            'Month',
        ];

        foreach ($form['fields'] as $key => $field) {
            if ($entry['form_id'] == '1' && in_array($field->label, $label)) {
                $body['notes'] .= $field->label . ': ' . $entry[$field->id] . '/n';
            } elseif (in_array($field->adminLabel, $crmFields) && $entry['form_id'] != '2') {
                $body[$field->adminLabel] = $entry[$field->id];
            }
        }

        if (!isset($body['firstName']) || $body['firstName'] == '') {
            $body['firstName'] = $_GET['first_name'];
        }

        if (!isset($body['lastName']) || $body['lastName'] == '') {
            $body['lastName'] = "Doe";
        }

        if (!isset($body['phone']) || $body['phone'] == '') {
            $body['phone'] = $_GET['phone'];
        }

        if (!isset($body['email']) || $body['email'] == '') {
            $body['email'] = $_GET['email'];
        }

        if (!isset($body['creditScore']) || $body['creditScore'] == '') {
            $body['creditScore'] = $_GET['credit-score'];
        }

        if ($body['monthlyIncome'] == 'none') {
            $body['monthlyIncome'] = 0;
        }

        if ($entry['form_id'] == '2') {

        } else {

            $this->push_to_crm($body);
        }
    }

    public function push_to_crm($body)
    {

        $post_url = $this->post_url;

        if (isset($body['vehicleType'])) {
            switch ($body['vehicleType']) {
                case 'Sedan':
                    $body['vehicleType'] = 'SEDAN';
                    break;
                case 'Suv':
                    $body['vehicleType'] = 'SUV';
                    break;
                case 'Truck':
                    $body['vehicleType'] = 'TRUCK';
                    break;
                case 'Minivan':
                    $body['vehicleType'] = 'MINIVAN';
                    break;
                case 'Hatchback':
                    $body['vehicleType'] = 'HATCHBACK';
                    break;
                case 'Convertible':
                    $body['vehicleType'] = 'CONVERTIBLE';
                    break;
                case 'Coupe':
                    $body['vehicleType'] = 'COUPE';
                    break;
                case 'Wagon':
                    $body['vehicleType'] = 'WAGON';
                    break;
                default:
                    $body['vehicleType'] = 'ANYTYPE';
            }
        } else {
            $body['vehicleType'] = 'ANYTYPE';
        }

        if (isset($body['creditScore'])) {
            switch ($body['creditScore']) {
                case 'Good (650+)':
                    $body['creditScore'] = 'GOOD';
                    break;
                case 'Poor (400-549)':
                    $body['creditScore'] = 'POOR';
                    break;
                case 'Fair (550-649)':
                    $body['creditScore'] = 'FAIR';
                    break;
                case 'Very Poor (399 or less)':
                    $body['creditScore'] = 'VERYPOOR';
                    break;
                case 'Current Bankruptcy':
                    $body['creditScore'] = 'BANKRUPTCY';
                    break;
                case 'No Credit':
                    $body['creditScore'] = 'NOCREDIT';
                    break;
                default:
                    $body['creditScore'] = 'UNKNOWN';
            }
        } else {
            $body['creditScore'] = 'UNKNOWN';
        }

        function phone_number_format($number)
        {
            // Allow only Digits, remove all other characters.
            $number = preg_replace("/[^\d]/", "", $number);

            // get number length.
            $length = strlen($number);

            // if number = 10
            if ($length == 10) {
                $number = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $number);
            }
            return $number;
        }
        if (isset($body['phone'])) {
            $body['phone'] = phone_number_format($body['phone']);
        }
        if (isset($body['phone'])) {
            $body['phone'] = phone_number_format($body['phone']);
        }
        if (isset($body['vin'])) {
            $body['trxType'] = 'DEAL';
        }
        $remote_get = wp_remote_get($post_url, array(
            'sslverify' => false,
            'timeout' => 20,
            'body' => $body,
        ));
        // var_dump($body);
        // var_dump($remote_get);

        // die;
    }
}
