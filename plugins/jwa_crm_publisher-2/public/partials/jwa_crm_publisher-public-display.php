<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       htts://justwebagency.com
 * @since      1.0.0
 *
 * @package    Jwa_crm_publisher
 * @subpackage Jwa_crm_publisher/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
