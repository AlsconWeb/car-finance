<?php

/**
 * Fired during plugin deactivation
 *
 * @link       htts://justwebagency.com
 * @since      1.0.0
 *
 * @package    Jwa_crm_publisher
 * @subpackage Jwa_crm_publisher/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Jwa_crm_publisher
 * @subpackage Jwa_crm_publisher/includes
 * @author     Ivan Karpushchenko <theblackkarps@gmail.com>
 */
class Jwa_crm_publisher_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
