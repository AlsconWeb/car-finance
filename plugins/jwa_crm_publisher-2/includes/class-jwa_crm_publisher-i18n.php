<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       htts://justwebagency.com
 * @since      1.0.0
 *
 * @package    Jwa_crm_publisher
 * @subpackage Jwa_crm_publisher/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Jwa_crm_publisher
 * @subpackage Jwa_crm_publisher/includes
 * @author     Ivan Karpushchenko <theblackkarps@gmail.com>
 */
class Jwa_crm_publisher_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'jwa_crm_publisher',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
