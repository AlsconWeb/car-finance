��    (      \  5   �      p     q  $   �  
   �     �     �     �     �     �     �          #  9   *  $   d  �   �          %     A     Q     a     x     �  I   �     �          
          ,     F     V     n     �  I   �     �     �  
   �     
     '     D     T  K  a  /   �
  6   �
       	         *     A     Q     ]  	   t     ~     �  N   �  $   �  �        �     �     �     �  %     %   =     c  U   ~     �  	   �     �               6     H     b  
   z  Z   �  	   �     �     �          (     E     W           $   
            	          !                    (             #                             %      &                                     "                            '       Add/update partial entry if Adds support for partial submissions All Fields Apply Conditional Logic Default Filter Enable Enable Condition Entry ListComplete Entry ListPartial Fields File Uploads will not be included in the partial entries. Gravity Forms Partial Entries Add-On In the interest of transparency and out of respect for users' privacy this notice will appear at the top of the form below the description. Last Saved: %s  Number of entries per page: Open saved form Partial Entries Partial Entries: Saved Partial Entries: Updated Partial Entry ID Please note that your information is saved on our server as you enter it. Process this feed if Progress Progress: %s Progress: all fields Progress: required fields Required Fields Save and Continue Token Save and Continue URL Saved The Credit Card field values will not be included in the partial entries. Threshold setting%s of %s Warning Message all fields https://www.gravityforms.com https://www.rocketgenius.com required fields rocketgenius Project-Id-Version: gravityformspartialentries
Report-Msgid-Bugs-To: https://www.gravtiyforms.com
POT-Creation-Date: 2018-12-18 17:13:46+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-05-26 02:11+0000
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Dutch (Netherlands) (https://www.transifex.com/rocketgenius/teams/30985/nl_NL/)
Language: nl_NL
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Zanata 4.6.2
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: utf-8
X-Textdomain-Support: yes
 Gedeeltelijke inzending toevoegen/bijwerken als Voegt ondersteuning toe voor gedeeltelijke inzendingen Alle velden Toepassen Voorwaardelijke logica Standaardfilter Inschakelen Voorwaarde inschakelen Voltooien Gedeeltelijk Velden Bestandsuploads zullen niet in de gedeeltelijke inzendingen worden bijgevoegd. Gravity Forms Partial Entries add-on In het belang van transparantie en uit respect voor de privacy van de gebruiker zal dit bericht aan de bovenkant van het formulier onder de beschrijving verschijnen. Laatst opgeslagen: %s Aantal inzendingen per pagina: Opgeslagen formulier openen Gedeeltelijke inzendingen Gedeeltelijke inzendingen: opgeslagen Gedeeltelijke inzendingen: bijgewerkt Gedeeltelijke inzending-ID Let er op dat je informatie zal worden opgeslagen op onze server zodra je het invult. Deze feed verwerken als Voortgang Voortgang: %s Voortgang: alle velden Voortgang: vereiste velden Verplichte velden Opslaan en doorgaan-token Opslaan en doorgaan-URL Opgeslagen De creditcard-veldwaarden zullen niet in deze gedeeltelijke inzendingen worden bijgevoegd. %s van %s Waarschuwingsbericht alle velden https://www.gravityforms.com https://www.rocketgenius.com verplichte velden rocketgenius 