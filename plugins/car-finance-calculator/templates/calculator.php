<!-- Car Finance Calc -->
<div id="jwa_car_calc" class="jwa_car_calc">
	<h3 class="carcalc_heading">Car finance calculator</h3>
	<form id="carcalc_form" class="carcalc_form">
		<div class="carcalc_col carcalc_col_left">
			<div class="carcalc_formgroup">
				<span id="carcalc_amount-preview" class="carcalc_range_preview"></span>
				<label for="carcalc_amount">Price of your new vehicle</label>
				<input class="carcalc_range" type="range" id="carcalc_amount" name="carcalc_amount" min="5000" max="100000" value="50000" step="1000">
			</div>
			<div class="carcalc_formgroup">
				<span id="carcalc_term-preview" class="carcalc_range_preview"></span>
				<label for="carcalc_term">Duration of your loan</label>
				<input class="carcalc_range" type="range" id="carcalc_term" name="carcalc_term" min="1" max="10" value="5" step="1">
			</div>
			<div class="carcalc_formgroup">
				<label for="carcalc_interest">Expected interest rate</label>
				<div id="carcalc_suffix_container">
					<input type="number" min=1 max=25 id="carcalc_interest" name="carcalc_interest" value=8>
					<span id="carcalc_suffix">%</span>
				</div>	
			</div>
			<div class="carcalc_formgroup">
				<label>Payment</label>
					<div class="carcalc_payment_inputs">
					<div class="d_inb">
												<input type="radio" checked id="carcalc_payment-weekly" value="weekly" name="carcalc_payment" >
													<label class="radio_label" for="carcalc_payment-weekly">Weekly</label>
						
											</div>
						<div class="d_inb"><input type="radio" id="carcalc_payment-biweekly" value="biweekly" name="carcalc_payment">
								<label class="radio_label"  for="carcalc_payment-biweekly">Bi-Weekly</label>
							
												</div>
					<div class="d_inb" >
							<input type="radio" id="carcalc_payment-monthly" value="monthly" name="carcalc_payment">
							<label class="radio_label monthly_label"  for="carcalc_payment-monthly">Monthly</label>
					</div>
					</div>
			</div>
			<!-- <button type="submit" class="btn carcalc_submit">
				Calculate
			</button> -->
		</div>
		<div class="carcalc_col carcalc_col_right">
			<div class="carcalc_result">
				<p class="carcalc_result-text">
					$
					<span id="carcalc_result">
					
					</span>
				</p>
				<p class="carcalc_result-sub">

				</p>
			</div>
			<a href="./secure-car-finance-application" class="btn carcalc_submit">
				Apply Now
			</a>
			<p class="carcalc_note">
				Note: Any and all results from this calculator are to be used only as an indication of potential financing options.
			</p>
			<a href="/calculator/" class="btn carcalc_submit btn_learn_more" style="display: none;">
				Learn more
			</a>
		</div>
	</form>
</div>
<!-- endcalc -->