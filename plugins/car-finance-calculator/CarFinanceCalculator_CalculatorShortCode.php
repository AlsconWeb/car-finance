<?php
include_once('CarFinanceCalculator_ShortCodeScriptLoader.php');
 
class CarFinanceCalculator_CalculatorShortCode extends CarFinanceCalculator_ShortCodeScriptLoader {
 
    static $addedAlready = false;
    public function handleShortcode($atts) {
        global $post;
        ob_start();
        $theme_dir = get_stylesheet_directory();
        include dirname( __FILE__ ) . '/templates/calculator.php';
        return ob_get_clean();
    }
 
    public function addScript() {
        if (!self::$addedAlready) {
            self::$addedAlready = true;
                wp_enqueue_style('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css');
                wp_enqueue_style('tax_calc_styles', plugins_url('css/styles.css', __FILE__));
                wp_enqueue_script('jquery');
                wp_enqueue_script('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js');
                wp_enqueue_script('tax_calc_scripts', plugins_url('js/scripts.js', __FILE__));

        }
    }
}