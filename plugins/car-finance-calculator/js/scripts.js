// Number postfix

'use strict';

var inputElement = document.getElementById('carcalc_interest');
var suffixElement = document.getElementById('carcalc_suffix');

inputElement.addEventListener('input', updateSuffix);

updateSuffix();

function updateSuffix() {
  var width = getTextWidth(inputElement.value, '18px Montserrat');
  suffixElement.style.left = width + 'px';
}

/**
 * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
 * 
 * @param {String} text The text to be rendered.
 * @param {String} font The css font descriptor that text is to be rendered with (e.g. "bold 14px verdana").
 * 
 * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
 */
function getTextWidth(text, font) {
  // re-use canvas object for better performance
  var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
  var context = canvas.getContext("2d");
  context.font = font;
  var metrics = context.measureText(text);
  return metrics.width;
}

jQuery(document).ready(function($){

    // Amount Values Update
    function setAmount(){
        $('#carcalc_amount-preview').text('$'+$('#carcalc_amount').val());
    }
    $('#carcalc_amount').change(setAmount);
    setAmount();

    // Term Values Update
    function setTerm(){
        var text = $('#carcalc_term').val()>1?' years':' year';
        $('#carcalc_term-preview').text($('#carcalc_term').val()+text);
    }
    $('#carcalc_term').change(setTerm);
    setTerm();

    // Submitting on each change
    $('#carcalc_form input').change(function(){
        $('#carcalc_form').submit();
    });
    // Submitting the form
    $('#carcalc_form').submit(function(event){
        event.preventDefault();
        var carcalc_amount = $('#carcalc_amount').val();
        var carcalc_term = $('#carcalc_term').val();
        var carcalc_interest = $('#carcalc_interest').val();
        var carcalc_payment = $('input[name="carcalc_payment"]:checked').val();
        if( carcalc_amount && carcalc_term && carcalc_interest && carcalc_payment ){
            var payment_frequency;
            switch (carcalc_payment) {
                case 'weekly':
                payment_frequency = 52;
                $('.carcalc_result-sub').text('Weekly Repayment');
                  break;
                case 'biweekly':
                payment_frequency = 26;
                $('.carcalc_result-sub').text('Bi-Weekly Repayment');
                  break;
                case 'monthly':
                payment_frequency = 12;
                $('.carcalc_result-sub').text('Monthly Repayment');
                  break;
                default:
                payment_frequency = 1;
              }
            // var result = Math.floor((carcalc_amount  / carcalc_term)*carcalc_interest/payment_frequency);
            var result = Math.round(((carcalc_amount*(carcalc_interest*0.01/payment_frequency))/(1-(1+(carcalc_interest*0.01/payment_frequency))**(-1*carcalc_term*payment_frequency)))*100)/100;
            result = new Intl.NumberFormat('en-CA').format(result.toFixed());
            console.log(result);
            $('#carcalc_result').text(result);
        }
    });
    $('#carcalc_form').submit();

});