<?php

/**
 * PenNews functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package PenNews
 */

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

add_filter( 'gform_field_content_1', function( $field_content, $field ) {
	// print_r($field);
    if ( $field->id == 60 ) {
		$html = "
		<p class='form_final_text'>
		I consent to receive electronic messages to my email addresses and mobile phone numbers (message and data rates may apply) from Car Finance Canada regarding Car Finance Canada’ products, services and related offerings.</p>
		<br>
		<p class='form_final_text'>
		You may withdraw your consent at any time by using an unsubscribe mechanism in an electronic message sent by Car Finance Canada.
		</p>";
        return str_replace( 'html_label', $html, $field_content );
    }
    return $field_content;
}, 10, 2 );

add_action( 'wp_enqueue_scripts', 'theme_custom_scripts' );
function theme_custom_scripts() {
	wp_enqueue_style( 'child_custom_styles', get_stylesheet_directory_uri(). '/assets/css/custom.css' );
	wp_enqueue_script( 'child_custom_js', get_stylesheet_directory_uri(). '/assets/js/custom.js' );
}

add_filter("gform_confirmation_anchor", create_function("","return false;"));

// custom gravity form field type applied to a specific form and field 
add_filter( 'gform_field_input_1_21', 'field_21_function', 10, 5 );
	
	function field_21_function( $input, $field, $value, $lead_id, $form_id ) {
		
		 if ( $form_id == 1 && $field->id == 21 ) {
	        $input = '<input id="input_1_21" class="small limited_number_4" tabindex="4" min="1920" max="2021" name="input_21" step="1" type="number" value="" aria-required="true" aria-invalid="false" />';
	    }
	    return $input;
	}

	add_filter( 'gform_field_input_1_23', 'field_23_function', 10, 5 );
	
	function field_23_function( $input, $field, $value, $lead_id, $form_id ) {
		
		 if ( $form_id == 1 && $field->id == 23 ) {
	        $input = '<input id="input_1_23" class="small limited_number_6" tabindex="4" min="0" max="1000000" name="input_23" step="1000" type="number" value="" aria-required="true" aria-invalid="false" />';
	    }
	    return $input;
	}

	add_filter( 'gform_field_input_1_28', 'field_28_function', 10, 5 );
	
	function field_28_function( $input, $field, $value, $lead_id, $form_id ) {
		
		 if ( $form_id == 1 && $field->id == 28 ) {
	        $input = '<input id="input_1_28" class="small limited_number_2" tabindex="4" min="0" max="99" name="input_28" step="1" type="number" value="" aria-required="true" aria-invalid="false" />';
	    }
	    return $input;
	}

	add_filter( 'gform_field_input_1_29', 'field_29_function', 10, 5 );
	
	function field_29_function( $input, $field, $value, $lead_id, $form_id ) {
		
		 if ( $form_id == 1 && $field->id == 29 ) {
	        $input = '<input id="input_1_29" class="small limited_number_2" tabindex="4" min="0" max="99" name="input_29" step="1" type="number" value="" aria-required="true" aria-invalid="false" />';
	    }
	    return $input;
	}

	add_filter( 'gform_field_input_1_44', 'field_44_function', 10, 5 );
	
	function field_44_function( $input, $field, $value, $lead_id, $form_id ) {
		
		 if ( $form_id == 1 && $field->id == 44 ) {
	        $input = '<input id="input_1_44" class="small limited_number_2" tabindex="4" min="0" max="99" name="input_44" step="1" type="number" value="" aria-required="true" aria-invalid="false" />';
	    }
	    return $input;
	}

	add_filter( 'gform_field_input_1_45', 'field_45_function', 10, 5 );
	
	function field_45_function( $input, $field, $value, $lead_id, $form_id ) {
		
		 if ( $form_id == 1 && $field->id == 45 ) {
	        $input = '<input id="input_1_45" class="small limited_number_2" tabindex="4" min="0" max="99" name="input_45" step="1" type="number" value="" aria-required="true" aria-invalid="false" />';
	    }
	    return $input;
	}

	add_filter( 'gform_notification', 'send_notification_filter', 10, 3 );
	function send_notification_filter( $notification, $form, $entry ) {
		if($notification['event'] == 'partial_entry_updated'){
			$time = time();
		$entry_id = $entry['partial_entry_id'];
		$json = get_option( 'gravity_notifications' );
		if ($json){
			$json = json_decode( $json );
			// error_log(print_r('entry_id_notttt',true));
			// error_log(print_r($form,true));
			$json->$entry_id = $time;
		} else $json = array( $entry['partial_entry_id'] => $time);



		update_option( 'gravity_notifications', json_encode( $json ) );
		// error_log(print_r('json',true));
		// error_log(print_r($json,true));

		wp_schedule_single_event( time() + 300, 'jwa_send_filtered_notification', array('partial_entry_updated',$form,$entry,$time,$entry_id) );
		} else return $notification;
		
	}
	// wp_clear_scheduled_hook( 'jwa_send_filtered_notification' );

	add_action( 'jwa_send_filtered_notification','send_notification', 100, 5 );
	function send_notification ( $event, $form, $entry, $time, $entry_id ) {
		$json_result = get_option( 'gravity_notifications' );
		$json_result = json_decode( $json_result );
		
		if(property_exists($json_result, $entry_id) && $json_result->$entry_id == $time){
			$message = "You have a new form submission from $entry[106]. Please follow the <a href='".site_url()."/wp-admin/admin.php?page=gf_entries&view=entry&id=1&lid=".$entry['id']."&order=ASC&filter&paged=1&pos=0&field_id&operator'>Link</a> for results.";
			$headers = array('Content-Type: text/html; charset=UTF-8');
			wp_mail( 'anton@justwebagency.com', 'You have a new form submission from '.$entry[106], $message, $headers );
			unset($json_result->$entry_id);
			update_option( 'gravity_notifications', json_encode( $json_result ) );

		} else {
			if(property_exists($json_result, $entry_id)){

			}
			
		}

		}

function call_banner( $atts ){
	return '
	<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row_60000749 vc_row-fluid penci-pb-row cta-2 vc_custom_1550587884210 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex vc_custom_1550587884223" style="padding-top: 30px;   padding-bottom: 30px; background-color: #312f46 !important;height:200px;">
    			<div class="wpb_column vc_column_container vc_col-sm-8 penci-col-8" style="display:flex;align-items:center;height:100%;">
    				<div class="vc_column-inner wpb_column vc_column_container vc_col-sm-8 penci-col-8 ">
    					<div class="wpb_wrapper">
    						<div class="wpb_text_column wpb_content_element ">
    							<div class="wpb_wrapper">
    								<p class="h2">Call Us Today <span style="color: #ffffff;">(866) 543-7997</span></p>
    								<p class="h3" style="margin-bottom:0;">Questions? We’re here to help!</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="wpb_column vc_column_container vc_col-sm-4 penci-col-4 vc_custom_1550587822476"  style="background-color: #312f46 !important;z-index:1;display: flex;align-items: center;height: 100%;">
    				<div class="vc_column-inner wpb_column vc_column_container vc_col-sm-4 penci-col-4 vc_custom_1550587822476 vc_custom_1550587822461" style="width:100%;">
    					<div class="wpb_wrapper">
    						<div class="wpb_text_column wpb_content_element ">
    							<div class="wpb_wrapper">
    								<p style="margin-bottom:0;"><a class="btn" href="https://www.car-finance.ca/secure-car-finance-application/">APPLY NOW</a></p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
	'; // никаких echo, только return
}
 
add_shortcode( 'call', 'call_banner' );

// Хуки
function true_add_mce_button() {
	// проверяем права пользователя - может ли он редактировать посты и страницы
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return; // если не может, то и кнопка ему не понадобится, в этом случае выходим из функции
	}
	// проверяем, включен ли визуальный редактор у пользователя в настройках (если нет, то и кнопку подключать незачем)
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'true_add_tinymce_script' );
		add_filter( 'mce_buttons', 'true_register_mce_button' );
	}
}
add_action('admin_head', 'true_add_mce_button');
 
// В этом функции указываем ссылку на JavaScript-файл кнопки
function true_add_tinymce_script( $plugin_array ) {
	$plugin_array['true_mce_button'] = get_stylesheet_directory_uri() .'/assets/js/call_button.js'; // true_mce_button - идентификатор кнопки
	return $plugin_array;
}
 
// Регистрируем кнопку в редакторе
function true_register_mce_button( $buttons ) {
	array_push( $buttons, 'true_mce_button' ); // true_mce_button - идентификатор кнопки
	return $buttons;
}

add_filter('wpcf7_spam', '__return_false');